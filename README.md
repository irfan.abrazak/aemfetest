# AemFeTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Notes

The application uses a proxy to connect to http://test-demo.aemenersol.com during development due to CORS error.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Build Electron

Run `npm run start:electron` to scaffold the application into a Desktop app with electron.