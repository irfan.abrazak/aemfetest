export interface LoaderState {
  show: boolean;
  complete: boolean;
}