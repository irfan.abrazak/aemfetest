import { Component, OnInit, OnDestroy } from '@angular/core'
import { Subscription } from 'rxjs'
import { LoaderService } from '../../services/loader.service'
import { LoaderState } from './loader'

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})

export class LoaderComponent implements OnInit, OnDestroy {

  show: boolean = false
  complete: boolean = false
  private subscription: Subscription

  constructor(
    private loaderService: LoaderService
  ) { }

  ngOnInit() {
    this.subscription = this.loaderService.loaderState
      .subscribe((state: LoaderState) => {
        this.show = state.show
        this.complete = state.complete
      })
  }

  ngOnDestroy() {
    this.subscription.unsubscribe()
  }

}