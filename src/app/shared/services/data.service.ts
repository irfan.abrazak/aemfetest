import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core'
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from "../../../environments/environment"

@Injectable()

export class DataService {

  API: string = environment.API_URL

  constructor(private http: HttpClient) { }

  getData() {
    return this.http
      .get(`${this.API}/api/dashboard`)
      .pipe(map((response: Response) => response),
        catchError(error => {
          return throwError(error);
        })
      )
  }

}