import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import jwt_decode from 'jwt-decode';
import * as moment from 'moment';

@Injectable()
export class LoginAccess implements CanActivate {

  constructor(
    private router: Router,
  ) { }

  canActivate() {

    const localJwt = localStorage.getItem('jwt')

    if (!localJwt) {

      return true

    } else {

      const decoded: any = jwt_decode(localJwt)
      const d = new Date(0)
      d.setUTCSeconds(decoded.exp)

      // if jwt has expired, logout
      if (moment().isAfter(moment(d))) {

        return true

      }

      this.router.navigateByUrl('/')
      return false

    }

  }

}

@Injectable()
export class PageAccess implements CanActivate {

  constructor(
    private router: Router,
  ) { }

  canActivate() {

    const localJwt = localStorage.getItem('jwt')

    if (!localJwt) {

      localStorage.clear()
      this.router.navigateByUrl('/login')
      return false

    } else {

      const decoded: any = jwt_decode(localJwt)
      const d = new Date(0)
      d.setUTCSeconds(decoded.exp)

      // if jwt has expired, logout
      if (moment().isAfter(moment(d))) {

        localStorage.clear()
        this.router.navigateByUrl('/login')
        return false

      }

      return true
    }

  }

}