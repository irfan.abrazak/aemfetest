import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Credentials } from '../modules/login/login.component';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  API: string = environment.API_URL

  constructor(
    private http: HttpClient
  ) { }

  login(credentials: Credentials) {
    return this.http.post(
      `${this.API}/api/account/login`,
      credentials,
      { headers: { 'exempt': 'true' } }
    )
      .pipe(map((response: Response) => response),
        catchError(error => {
          console.log(error.message)
          return throwError(error)
        })
      )
  }

  getAccessToken() {
    return localStorage.getItem('jwt');
  }

}
