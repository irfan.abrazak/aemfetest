import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpInterceptor,
  HttpErrorResponse,
  HttpEvent,
  HttpResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { tap, catchError, finalize } from 'rxjs/operators';
import { AuthenticationService } from './authentication.service';
import { LoaderService } from 'src/app/shared/services/loader.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptor implements HttpInterceptor {

  private totalRequests = 0;
  private spinnerRef: any;

  private onEnd(): void {
    this.hideLoader();
  }

  private showLoader(): void {
    this.loaderService.show();
  }

  private hideLoader(): void {
    this.loaderService.hide();
    this.loaderService.complete();
    setTimeout(() => {
      this.loaderService.completeHide();
    }, 250);
  }

  constructor(
    private auth: AuthenticationService,
    private loaderService: LoaderService,
    private router: Router
  ) { }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<any> {

    if (
      this.spinnerRef == undefined
      && !request.headers.get('exempt')
    ) {
      this.showLoader();
      this.spinnerRef = true;
    }

    this.totalRequests++;
    request = this.addAuthHeader(request);

    return next.handle(request)
      .pipe(
        tap((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) { }
        }, (err) => {
          console.log(err)
        }),
        catchError((error: HttpErrorResponse) => {

          if (error.status === 401) {

            localStorage.clear()
            this.router.navigateByUrl('/login')

          }

          return throwError(error);

        }),
        finalize(() => {

          this.totalRequests--;
          if (this.totalRequests === 0) {
            if (this.spinnerRef != undefined) {
              this.onEnd();
            }
            this.spinnerRef = undefined;
          }

        }),
      )

  }

  addAuthHeader(request: HttpRequest<any>) {

    const token = this.auth.getAccessToken();

    if (token) {
      return request.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`
        },
        withCredentials: true
      })
    }
    return request;

  }

}