import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/authentication.service';

export interface Credentials {
  username: string;
  password: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  model: Credentials;

  @ViewChild('MyForm') MyForm: NgForm;

  constructor(
    private authService: AuthenticationService,
    private router: Router
  ) {
    this.model = ({} as Credentials)
  }

  errorMessage: string;
  submitForm() {
    console.log(this.MyForm.form.valid);
    console.log(this.MyForm)

    this.authService.login(this.MyForm.value)
      .subscribe({
        next: (res: any) => {
          console.log(res)
          localStorage.setItem('jwt', res)
          this.router.navigateByUrl('/');
        },
        error: (err: any) => {
          console.log(err)
          this.errorMessage = err.message
        }
      })

  }

}
