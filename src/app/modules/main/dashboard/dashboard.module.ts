import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { DataService } from 'src/app/shared/services/data.service';
import { DonutComponent } from './components/donut/donut.component';
import { BarComponent } from './components/bar/bar.component';
import { TableComponent } from './components/table/table.component';
import { ChartsModule } from 'ng2-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent
  },
];

@NgModule({
  declarations: [
    DashboardComponent,
    DonutComponent,
    BarComponent,
    TableComponent
  ],
  imports: [
    CommonModule,
    ChartsModule,
    NgbModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    DataService
  ]
})
export class DashboardModule { }
