export interface ChartSeries {
  name: string,
  value: number
}