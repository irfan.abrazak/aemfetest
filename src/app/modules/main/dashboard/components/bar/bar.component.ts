import { Component, Input, SimpleChanges, ViewChild } from '@angular/core';
import { ChartSeries } from '../../models/ChartSeries';
import { BaseChartDirective } from 'ng2-charts';

export interface BarChartSeries {
  data: number[],
  label: string
}

@Component({
  selector: 'app-bar',
  templateUrl: './bar.component.html',
  styleUrls: ['./bar.component.scss']
})
export class BarComponent {

  @Input() Series: ChartSeries
  @ViewChild(BaseChartDirective) baseChart: BaseChartDirective;

  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels = [];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData: Array<BarChartSeries> = [{
    data: [],
    label: null
  }]

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {

    if (!!changes.Series.currentValue) {
      this.barChartLabels = changes.Series.currentValue.map((x: any) => x.name)
      this.barChartData = [{
        data: changes.Series.currentValue.map((x: any) => x.value),
        label: 'Bar'
      }]
    }

  }

}