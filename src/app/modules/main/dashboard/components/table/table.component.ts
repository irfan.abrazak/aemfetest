import { Component, Input, OnInit } from '@angular/core';
import { TableData } from '../../models/TableData';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  @Input() tableUsers: TableData[] = []

  page = 1;
  pageSize = 10;
  // items: TableData[]

  constructor() { }

  ngOnInit() {
  }

}
