import { Component, Input, SimpleChanges, ViewChild } from '@angular/core';
import { ChartSeries } from '../../models/ChartSeries';
import { BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'app-donut',
  templateUrl: './donut.component.html',
  styleUrls: ['./donut.component.scss']
})
export class DonutComponent {

  @Input() Series: ChartSeries
  @ViewChild(BaseChartDirective) baseChart: BaseChartDirective;

  public donutChartLabels = []
  public donutChartData = []
  public donutChartType = 'doughnut'

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {

    if (!!changes.Series.currentValue) {
      this.donutChartLabels = changes.Series.currentValue.map((x: any) => x.name)
      this.donutChartData = changes.Series.currentValue.map((x: any) => x.value)
    }

  }

}
