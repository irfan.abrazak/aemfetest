import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DataService } from 'src/app/shared/services/data.service';
import { ChartSeries } from './models/ChartSeries';
import { TableData } from './models/TableData';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(
    private dataService: DataService
  ) { }

  private _data: Subscription
  donutChartData: ChartSeries
  barChartData: ChartSeries
  tableUsers: TableData

  ngOnInit() {

    this._data = this.dataService
      .getData()
      .subscribe({
        next: (res: any) => {
          this.donutChartData = res.chartDonut
          this.barChartData = res.chartBar
          this.tableUsers = res.tableUsers
        },
        error: (err: any) => {
          console.log(err)
        }
      })

  }

  ngOnDestroy() {
    if (!this._data) {
      this._data.unsubscribe()
    }
  }

}
