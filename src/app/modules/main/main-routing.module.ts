import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageAccess } from 'src/app/core/page.guard';
import { MainComponent } from './main.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full'
      },

      {
        path: "dashboard",
        loadChildren: './dashboard/dashboard.module#DashboardModule',
        canActivate: [PageAccess]
      },
      {
        path: '**',
        redirectTo: 'dashboard',
        pathMatch: 'full',
      },
    ]
  },
  {
    path: '**',
    component: MainComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }